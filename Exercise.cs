using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;

namespace WorkoutRecorder {
    class Exercise {
        String name;
        String suggested;
        String[] notes;
        Exercise(String name, String suggested, String[] notes) {
            this.name = name;
            this.suggested = suggested;
            this.notes = notes;
        }
    }
}