using Google.Apis.Auth.OAuth2;
using Google.Apis.Sheets.v4;
using Google.Apis.Sheets.v4.Data;
using Google.Apis.Services;
using Google.Apis.Util.Store;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;


namespace spreadsheet_testing
{
    class Program
    {
        static String spreadsheetId;
        static int dataStartRow;

        // If modifying these scopes, delete your previously saved credentials
        // at ~/.credentials/sheets.googleapis.com-dotnet-quickstart.json
        static string[] Scopes = { SheetsService.Scope.SpreadsheetsReadonly };
        static string ApplicationName = "Google Sheets API .NET Quickstart";

        static void ProcessDay() {

        }

        static void ProcessWeek() {
            UserCredential credential;

            using (var stream =
                new FileStream("credentials.json", FileMode.Open, FileAccess.Read))
            {
                // The file token.json stores the user's access and refresh tokens, and is created
                // automatically when the authorization flow completes for the first time.
                string credPath = "token.json";
                credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
                    GoogleClientSecrets.Load(stream).Secrets,
                    Scopes,
                    "user",
                    CancellationToken.None,
                    new FileDataStore(credPath, true)).Result;
                Console.WriteLine("Credential file saved to: " + credPath);
            }

            // Create Google Sheets API service.
            var service = new SheetsService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = ApplicationName,
            });

            String range = "Copy of Free Training Log!A" + dataStartRow + ":A";
            SpreadsheetsResource.ValuesResource.GetRequest request =
                    service.Spreadsheets.Values.Get(spreadsheetId, range);

            // Prints the names and majors of students in a sample spreadsheet:
            // https://docs.google.com/spreadsheets/d/1svfD8rkSzhztyYRFjZYkO7N8oACQgFXPWbI_nJLmzGM/edit
            ValueRange response = request.Execute();
            IList<IList<Object>> values = response.Values;
            if (values != null && values.Count > 0)
            {
                Console.WriteLine("Day");
                for (var rowIndex = 0; rowIndex < values.Count; rowIndex++)
                {
                    var row = values[rowIndex];
                    // Indicates that the section for a new day has started
                    if (row.Count > 0) {
                        Console.WriteLine("{0} on row {1}", row[0], rowIndex + dataStartRow);
                    }
                }
            }
            else
            {
                Console.WriteLine("No data found.");
            }
            Console.Read();
        }

        /*static void Main(string[] args)
        {
            

            // Define request parameters.
            spreadsheetId = "1svfD8rkSzhztyYRFjZYkO7N8oACQgFXPWbI_nJLmzGM";
            dataStartRow = 3;
            ProcessWeek();
        }*/
    }
}
